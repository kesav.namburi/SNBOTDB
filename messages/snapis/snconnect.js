module.exports = BasicAuth;

function BasicAuth(snInstanceURL, uri, username, password, options) {
	this.snInstanceURL = snInstanceURL;
	this.username = username;
	this.uri = uri;
	this.password = password;
    this.options = options;
}

BasicAuth.prototype.authenticate = function(callBack) {
	var request = require('request');
    
	request({
		baseUrl : this.snInstanceURL,
		method : 'GET',
		uri : this.uri,
		json : true,
		// Here we use the basic authentication. The username and password set here will send 
		// as the authentication header.
		auth: {
            'user': this.username,
            'pass': this.password,
            'sendImmediately': true
        }
	}, 
	
	function(err, response, body) {
		if (!err && response.statusCode == 200){
			callBack(err, response, body, response.headers['set-cookie']);
		} else {
			callBack(err, response, body);
		}
	});
}