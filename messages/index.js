/*-----------------------------------------------------------------------------
To learn more about this template please visit
https://aka.ms/abs-node-proactive 
-----------------------------------------------------------------------------*/
"use strict";
var http = require('http');
var builder = require("botbuilder");
var botbuilder_azure = require("botbuilder-azure");
var azure = require('azure-storage');
var path = require('path');

var useEmulator = (process.env.NODE_ENV == 'development');

var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
    appId: process.env['MicrosoftAppId'],
    appPassword: process.env['MicrosoftAppPassword'],
    stateEndpoint: process.env['BotStateEndpoint'],
    openIdMetadata: process.env['BotOpenIdMetadata']
});

var bot = new builder.UniversalBot(connector);
bot.localePath(path.join(__dirname, './locale'));

// Intercept trigger event (ActivityTypes.Trigger)
bot.on('trigger', function (message) {
	//var card = createNotification();
    // handle message from trigger function
    var queuedMessage = message.value;
	
	fetchUserApprovals(queuedMessage);
});

var userStore = [];

// Handle message from user
bot.dialog('/', function (session) {
    // add message to queue
    session.sendTyping();
	var card = createSNRootOptions(session);

	// attach the card to the reply message

	var msg = new builder.Message(session).addAttachment(card);

    session.send(msg);
	
	var address = session.message.address;

    userStore.push(address);

});

var queueSvc = azure.createQueueService(process.env.AzureWebJobsStorage);
queueSvc.createQueueIfNotExists('bot-queue', function(err, result, response){
	if(!err){
		setInterval(function () {
			userStore.forEach(function(address){
				var queuedMessage = { address: address };
				// Add the message to the queue
				var queueMessageBuffer = new Buffer(JSON.stringify(queuedMessage)).toString('base64');
				queueSvc.createMessage('bot-queue', queueMessageBuffer, function(err, result, response){
					if(!err){
						// Message inserted
						//session.send('Checking for approvals!!'); 
					} else {
						// this should be a log for the dev, not a message to the user
						//session.send('There was an error inserting your message into queue');
					}
				});
			});
		}, 60000);
	} else {
		// this should be a log for the dev, not a message to the user
		session.send('There was an error creating your queue');
	}
});
/*setInterval(function () {
    userStore.forEach(function (address) {
        var newConversationAddress = Object.assign({}, address);

        //delete newConversationAddress.conversation;
 
		// start survey dialog
        bot.beginDialog(newConversationAddress, 'fetchnotifications', null, function (err) {
            if (err) {
                // error ocurred while starting new conversation. Channel not supported?
                bot.send(new builder.Message()
                    .text('This channel does not  support this operation: ' + err.message)
                    .address(address));
            }
        }); 
    });
}, 60000);*/



bot.dialog('fetchnotifications', function (session) {
    fetchUserApprovals(session);
});

bot.dialog('OpenTickets', function (session) {
    getOpenTickets(session);
    //session.send("You clicked open tickets").endDialog();
}).triggerAction({ matches: /OpenTickets/i });

bot.dialog('ExistingTicket', [
    function (session) {
        builder.Prompts.text(session, "Please enter the ticket Number?");
    },
    function (session, results) {
        session.userData.ticketNumber = results.response;
        session.send("Checking Ticket: " + session.userData.ticketNumber + " status ....");
		getTicketStatus(session,session.userData.ticketNumber);
    }
]).triggerAction({ matches: /ExistingTicket/i });

bot.dialog('RaiseRequest', [
    function (session) {
		
		session.send(new builder.Message(session).addAttachment(patientMsg()));//
        builder.Prompts.text(session, "Please be specific with your request (ex: Reset my password or book a conference room)");
    },
    function (session, results) {
        session.userData.ticketNumber = results.response;
        session.send("Checking Ticket: " + session.userData.ticketNumber + " status ....");
		getTicketStatus(session,session.userData.ticketNumber);
    }
]).triggerAction({ matches: /Other/i });

function createSNRootOptions(session) {
	var currentTime = new Date();
	var hours = currentTime.getHours();
	var message = "Good Evening ";
	
	if(hours < 12)
		message = "Good Morning ";
	else if(hours >= 12 && hours < 16)
		message = "Good Afternoon ";
	
   return new builder.ThumbnailCard(session)
        .title(message)
        .subtitle('Hello ' + session.message.address.user.name)
        .text('I am SNOW BOT and how can I help you?')
        .images([
            builder.CardImage.create(session, 'https://lh3.googleusercontent.com/hq4O2SOyPKZK8pf81aaJRs69svpIQFu8Tb4u5WUXxGGjSHpy0pvXB0Y7f6jkOzVj6ZU=w300')
        ])
        .buttons([
			builder.CardAction.imBack(session, "OpenTickets", "My Open Tickets"),
			builder.CardAction.imBack(session, "ExistingTicket", "Status on Ticket"),
			builder.CardAction.imBack(session, "Other", "Raise a Request")
        ]);
}

function getOpenTickets(session){
	try{
		session.send('Fetching your open tickets...');
		
		var BasicAuth = require('./snapis/snconnect');

		var client = new BasicAuth('https://dev27932.service-now.com', 
									'/api/now/table/sc_req_item?sysparm_query=active%3Dtrue&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_suppress_pagination_header=true&sysparm_fields=number%2Ccat_item%2Cstage%2Copened_at%2Cdue_date%2Csys_id&sysparm_limit=10',
									'admin', 'Shivkes@(*3');

		client.authenticate(function(err, response, body, cookie) {
			var ritms = body.result;
			var msg = {
				type: 'message',
				attachmentLayout: 'list',
				text: "Your Open Requests"
			};
			
			var ritmAttachments = [];
			
			for(var index = 0, len =  ritms.length; index <len; index++){
				var ritm = ritms[index];
				var card = getRITMCard(ritm);

				ritmAttachments.push(card);
			}
			msg.attachments = ritmAttachments;
			
			session.send(msg).endDialog();
		});
		
	}catch(err){
		session.send(err.message).endDialog();
	}
}

function fetchUserApprovals(queuedMessage){
	try{
		var BasicAuth = require('./snapis/snconnect');

		var client = new BasicAuth('https://dev27932.service-now.com', 
									'/api/94167/approvals',
									'admin', 'Shivkes@(*3');

		client.authenticate(function(err, response, body, cookie) {
			var approvals = body.result;
			
			if(approvals.length ==0 )
				return;
			var msg = {
				type: 'message',
				attachmentLayout: 'list',
			};
			
			var apprAttachments = [];
			
			for(var index = 0, len =  approvals.length; index <len; index++){
				var approval = approvals[index];
				var card = createNotification(approval);

				apprAttachments.push(card);
			}
			msg.attachments = apprAttachments;
			var reply = new builder.Message()
				.address(queuedMessage.address)
				//.text("test")
				.attachments(apprAttachments);
			bot.send(reply);
		});
	}catch(err){
		//session.send(err.message).endDialog();
	}
}

function getTicketStatus(session,ticketNumber){
	try{
		var BasicAuth = require('./snapis/snconnect');

		var client = new BasicAuth('https://dev27932.service-now.com', 
									'/api/now/table/sc_req_item?sysparm_query=number%3D' + ticketNumber + '&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_suppress_pagination_header=true&sysparm_fields=number%2Ccat_item%2Cstage%2Copened_at%2Cdue_date%2Csys_id&sysparm_limit=10',
									'admin', 'Shivkes@(*3');
		
		client.authenticate(function(err, response, body, cookie) {
			var ritms = body.result;
			
			var msg = {
				type: 'message',
				attachmentLayout: 'list',
				text: (ritms.length > 0) ? ("Ticket Status: " +ticketNumber) : ("Invalid Ticket: " +ticketNumber)
			};
			
			var ritmAttachments = [];
			
			for(var index = 0, len =  ritms.length; index <len; index++){
				var ritm = ritms[index];
				var card = getRITMCard(ritm);

				ritmAttachments.push(card);
			}
			msg.attachments = ritmAttachments;
			
			session.send(msg).endDialog();
		});
		
	}catch(err){
		session.send(err.message).endDialog();
	}
}

function getRITMCard(ritmObj){
	var response = {
      "contentType": "application/vnd.microsoft.card.hero",
      "content": {
        "title": ritmObj.number,
        "subtitle": ritmObj.cat_item,
		"text": ritmObj.cat_item,
        "buttons": [
          {
            "type": "openUrl",
            "title": "Open",
            "value": "https://dev27932.service-now.com/nav_to.do?uri=sc_req_item.do?sys_id=" + ritmObj.sys_id
          }
        ]
      }
	};
	return response;
}

function patientMsg(){
	return {
      "contentType": "application/vnd.microsoft.card.thumbnail",
      "content": {
        "text": "Please be patient as I am still learning, in case i can't able to help you i will redirect to my human friends who will able to help you for sure.",
        "images": [
          {
            "url": "https://static.comicvine.com/uploads/original/11124/111247756/5020954-walle.jpg"
          }
        ]
      }
    };
}
function createNotification(approval) {
	return {
      "contentType": "application/vnd.microsoft.card.thumbnail",
      "content": {
        "title": "Action Required",
        "subtitle": approval.number,
		"text": approval.description,
        "images": [
          {
            "url": "https://lh3.googleusercontent.com/hq4O2SOyPKZK8pf81aaJRs69svpIQFu8Tb4u5WUXxGGjSHpy0pvXB0Y7f6jkOzVj6ZU=w300"
          }
        ],
        "buttons": [
          {
            "type": "openUrl",
            "title": "Approve",
            "value": "www.google.com"
          },
          {
            "type": "openUrl",
            "title": "Reject",
            "value": "www.google.com"
          }
        ]
      }
    };
	}
	
if (useEmulator) {
    var restify = require('restify');
    var server = restify.createServer();
    server.listen(3978, function() {
        console.log('test bot endpont at http://localhost:3978/api/messages');
    });
    server.post('/api/messages', connector.listen());    
} else {
    module.exports = { default: connector.listen() }
}